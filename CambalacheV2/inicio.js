var ProductList = [];
window.onload = cargarLista();
let userLog = sessionStorage.getItem('userLogueado');

/**
 * Este metodo carga la lista
 */
function cargarLista() {
    const storageList = localStorage.getItem('ListProducts');
    if (storageList == null) {
        ProductList = [];

    } else {
        ProductList = JSON.parse(storageList);

    }
    return ProductList;
}
/**
 * 
 * @param {Producto a selepcionar} item 
 */
function productselect(item) {
    if (item !== null) {
        localStorage.setItem("ProductSelect", JSON.stringify(item));
        let estado = true;
        localStorage.setItem("Inicio", estado);
        window.location.href = 'http://sp.net/DetalleProducto.html';

    }
}



/**
 * Este metodo carga los productos en un carrusel
 */
function corrusel() {
    const product = document.getElementById('carousel');
    const fragment = document.createDocumentFragment();

    let newList = ProductList.splice(-3);


    for (let index = 0; index < newList.length; index++) {

        let targetas = document.createElement('div');

        if (index === 0) {
            targetas.className = 'carousel-item active text-center';
        } else {
            targetas.className = 'carousel-item text-center';
        }
        fragment.appendChild(targetas);

        let img = document.createElement('img');
        img.className = 'd-block w-100 ';
        img.setAttribute('src', newList[index].direccion);
        img.setAttribute('height', '550');
        targetas.appendChild(img);



        img.addEventListener('click', function() {
            console.log(newList[index]);
            productselect(newList[index]);


        });




    }

    product.appendChild(fragment);
}

/**
 * Este metodo valida si el usuario esta logeado
 */
function UserLog() {
    if (userLog != null) {
        if (userLog == 'true') {
            const line1 = document.getElementById('hIngoDas');
            const line2 = document.getElementById('fIngoDas');
            line1.innerText = 'Dashboard';
            line2.innerText = 'Dashboard';
            line1.setAttribute('href', 'http://sp.net/Dashboard.html');
            line2.setAttribute('href', 'http://sp.net/Dashboard.html');

        }
    }
}




/**
 * Este metodo carga el docuemnto
 */
$(document).ready(function() {

    corrusel();
    UserLog();
    console.log(userLog)

    $('#button-nav').click(function() {

        $('#line1').hide();
        $('#line2').hide();


    })

    if (screen.width <= 375 && userLog) {
        $('li').css({ 'margin': '-4px' });

    }

    $(window).resize(function() {


        if (screen.width <= 375) {
            $('li').css({ 'margin': '-4px' });

        }
        $('#line1').show();
        $('#line2').show();

    })

    $('#search').keyup(function(e) {
        const data = $('#search').val();
        const lis = document.getElementById('busqueda');
        const fragment = document.createDocumentFragment();
        lis.innerHTML = '';
        var newList = [];
        if (localStorage.getItem('ListProducts')) {
            newList = JSON.parse(localStorage.getItem('ListProducts'));

        }

        for (let index = 0; index < newList.length; index++) {
            if (newList[index].nombre.toLowerCase().startsWith(data.toLowerCase())) {
                let li = document.createElement('li');
                li.setAttribute("style", "list-style: none;");
                li.innerHTML = newList[index].nombre;
                li.addEventListener('click', function() {
                    productselect(newList[index]);
                })
                fragment.appendChild(li);



            } else {
                lis.innerHTML = '';
            }

        }
        lis.appendChild(fragment);

        if ($('#search').val().length == 0) {
            lis.innerHTML = '';
        }

    })


    if ($('#search').val().length == 0) {
        document.getElementById('busqueda').innerHTML = '';
    }


});



// background-image: url(/search.png);
//     background-repeat: no-repeat;