var ProductList = [];
window.onload = cargarLista();
let userLog = sessionStorage.getItem('userLogueado');


function cargarLista() {
    const storageList = localStorage.getItem('ListProducts');
    if (storageList == null) {
        ProductList = [];

    } else {
        ProductList = JSON.parse(storageList);

    }
    return ProductList;
}

function productselect(item) {
    if (item !== null) {
        localStorage.setItem("ProductSelect", JSON.stringify(item));
        let estado = false;
        localStorage.setItem("Inicio", estado);
        window.location.href = 'http://sp.net/DetalleProducto.html';

    }
}


function productos() {
    const product = document.getElementById('Productos');



    const fragment = document.createDocumentFragment();
    for (let index = 0; index < ProductList.length; index++) {


        let card = document.createElement('div');
        card.className = 'div class="col mb-4   col-md-6  col-lg-4';
        card.setAttribute("style", "max-height: 18rem;");

        let cardDiv = document.createElement('div');
        cardDiv.className = 'card d-flex flex-row h-100';
        card.appendChild(cardDiv);


        let cardbody = document.createElement('div');
        cardbody.className = 'card-body w-100 ';
        cardDiv.appendChild(cardbody);

        let img = document.createElement('img');
        img.className = 'card-img-top ';
        img.setAttribute('src', ProductList[index].direccion);

        cardbody.appendChild(img);

        let cardbody2 = document.createElement('div');
        cardbody2.className = 'card-body d-flex flex-column w-75 ';
        cardDiv.appendChild(cardbody2);

        //Nombre
        let name = document.createElement('a');
        name.className = 'product';
        name.setAttribute('style', 'color:#c95555;');
        name.setAttribute('href', '#');
        name.innerText = ProductList[index].nombre;

        cardbody2.appendChild(name);


        name.addEventListener('click', function() {
            console.log(ProductList[index]);
            productselect(ProductList[index]);


        });



        //Nombre
        let nameU = document.createElement('a');
        nameU.className = 'NameUser mt-1';
        nameU.setAttribute('style', 'color:#c95555;');
        nameU.innerText = ProductList[index].user.nombre;

        cardbody2.appendChild(nameU);


        fragment.appendChild(card);

    }
    product.appendChild(fragment);


}

function UserLog() {
    if (userLog != null) {
        if (userLog == 'true') {
            const line1 = document.getElementById('hIngoDas');
            const line2 = document.getElementById('fIngoDas');
            line1.innerText = 'Dashboard';
            line2.innerText = 'Dashboard';
            line1.setAttribute('href', 'http://sp.net/Dashboard.html');
            line2.setAttribute('href', 'http://sp.net/Dashboard.html');

        }
    }
}



$(document).ready(function() {
    cargarLista();
    productos();
    UserLog();



    $('#button-nav').click(function() {

        $('#line1').hide();
        $('#line2').hide();


    })

    if (screen.width <= 375 && userLog) {
        $('li').css({ 'margin': '-4px' });

    }

    $(window).resize(function() {


        if (screen.width <= 375) {
            $('li').css({ 'margin': '-4px' });

        }
        $('#line1').show();
        $('#line2').show();

    })

});