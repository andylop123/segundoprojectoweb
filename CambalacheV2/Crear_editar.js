var ProductList = [];
window.onload = cargarLista();

class Producto {
    constructor(id, user, nombre, descripcion, direccion, busco) {
        this.id = id;
        this.user = user;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.direccion = direccion;
        this.busco = busco;

    }
}




function addProductToSystem() {
    const nombre = document.getElementById('product-name').value;
    const userLog = JSON.parse(localStorage.getItem("UserLog"));
    const descripcion = document.getElementById('Description').value;
    const direccion = document.getElementById('url').value;
    const busco = document.getElementById('search').value;


    const producto = new Producto(id(), userLog, nombre, descripcion, direccion, busco);
    let editando = localStorage.getItem("Editando");

    if (editando !== 'true') {


        ProductList.push(producto);
        addListToLs(ProductList);
        location.href = 'http://sp.net/Dashboard.html';

    } else {
        editar(nombre, descripcion, direccion, busco);
        location.href = 'http://sp.net/Dashboard.html';

    }







}


function CargarDatos() {
    let productSe = JSON.parse(localStorage.getItem("ProductSelect"));
    let editando = localStorage.getItem("Editando");

    console.log(editando);

    if (productSe !== null) {
        if (editando == 'true') {
            console.log(productSe);
            console.log(editando);
            document.getElementById('product-name').value = productSe.nombre;
            document.getElementById('Description').value = productSe.descripcion;
            document.getElementById('url').value = productSe.direccion;
            document.getElementById('search').value = productSe.busco;


        }

    }

}



function editar(nombre, descripcion, direccion, busco) {
    let productSe = JSON.parse(localStorage.getItem("ProductSelect"));

    for (let item of ProductList) {
        if (productSe.id == item.id) {
            item.nombre = nombre;
            item.descripcion = descripcion;
            item.direccion = direccion;
            item.busco = busco;
            addListToLs(ProductList);
        }


    }
}


function addListToLs(list) {
    localStorage.setItem("ListProducts", JSON.stringify(list));

}

function cargarLista() {
    const storageList = localStorage.getItem("ListProducts");
    if (storageList == null) {
        ProductList = [];

    } else {
        ProductList = JSON.parse(storageList);

    }
    return ProductList;
}


function id() {


    if (ProductList.length == 0) {

        return 1;
    } else {
        return ProductList[ProductList.length - 1].id + 1;
    }



}





$(document).ready(function() {

    CargarDatos();

    $('#button-Register').click(() => {
        addProductToSystem();


    });


    $('#button-Cancel').click(function(event) {

        event.preventDefault();
        window.location.href = 'http://sp.net/Dashboard.html';


    })


    $('#button-nav').click(function() {

        $('#line1').hide();
        $('#line2').hide();


    })

    if (screen.width <= 375) {
        $('li').css({ 'margin': '-4px' });

    }


    $(window).resize(function() {


        if (screen.width <= 375) {
            $('li').css({ 'margin': '-4px' });

        }
        $('#line1').show();
        $('#line2').show();

    })








});



//Editar