// // window.onload = cargarLista();
var ProductList = [];
cargarLista();


function cargarLista() {
    const storageList = localStorage.getItem("ListProducts");
    if (storageList == null) {
        ProductList = [];

    } else {
        ProductList = JSON.parse(storageList);

    }
    return ProductList;
}

function name() {

    const user = JSON.parse(localStorage.getItem('UserLog'));

    if (user !== null) {
        const userLog = document.getElementById('name');
        userLog.innerHTML = user.nombre;
        console.log(user.nombre)
    }
}

// function Productos() {
//     const product = document.getElementById('Productos');
//     product.innerHTML = "";
//     let card;
//     for (let i = 0; i < ProductList.length; i++) {
//         product.innerHTML += '<div class="col mb-4   col-md-6  col-lg-4 " style="max-height: 18rem;"> ' +
//             '<div class="card d-flex flex-row h-100">' +
//             '<div class="card-body w-100 ">' +
//             '<img src="' + ProductList[i].direccion + '" class="card-img-top" alt="...">' +
//             '</div>' +
//             '<div class="card-body d-flex flex-column w-75" id="card-body2"> ' +
//             '<a  class="product" style="color:#c95555;" id="product" name="product">' + ProductList[i].nombre + '</a>' +
//             '<input class="datos" type="text" value="' + ProductList[i].id + '" style="display: none;"></input>' +
//             '<button type="button" class="btn bg-success text-white btn-sm  mt-1 button-editar" id="button-editar">Editar</button>' +
//             '<button type="button " class="btn   text-white bg-danger btn-sm  mt-4"  id="button-Eliminar">Eliminar</button>' +
//             '</div>' +
//             '</div>' +
//             '</div>';

//     }

// }





function productos() {
    const product = document.getElementById('Productos');
    let userLog = JSON.parse(localStorage.getItem('UserLog'));

    let button;
    const fragment = document.createDocumentFragment();
    for (let index = 0; index < ProductList.length; index++) {

        if (ProductList[index].user != null) {
            if (ProductList[index].user.id == userLog.id) {
                let card = document.createElement('div');
                card.className = 'div class="col mb-4   col-md-6  col-lg-4';
                card.setAttribute("style", "max-height: 18rem;");

                let cardDiv = document.createElement('div');
                cardDiv.className = 'card d-flex flex-row h-100';
                card.appendChild(cardDiv);


                let cardbody = document.createElement('div');
                cardbody.className = 'card-body w-100 ';
                cardDiv.appendChild(cardbody);

                let img = document.createElement('img');
                img.className = 'card-img-top ';
                img.setAttribute('src', ProductList[index].direccion);

                cardbody.appendChild(img);

                let cardbody2 = document.createElement('div');
                cardbody2.className = 'card-body d-flex flex-column w-75 ';
                cardDiv.appendChild(cardbody2);

                //Nombre
                let name = document.createElement('a');
                name.className = 'product';
                name.setAttribute('style', 'color:#c95555;');
                name.innerText = ProductList[index].nombre;

                cardbody2.appendChild(name);



                //buttons
                button = document.createElement('button');
                button.className = 'btn bg-success text-white btn-sm  mt-1';
                button.type = 'button';
                button.textContent = 'Editar';
                button.addEventListener('click', function() {
                    productselect(ProductList[index]);

                });
                cardbody2.appendChild(button);

                //buttons
                button2 = document.createElement('button');
                button2.className = 'btn   text-white bg-danger btn-sm  mt-4';
                button2.type = 'button';
                button2.textContent = 'Eliminar';
                button2.addEventListener('click', function() {
                    Eliminar(ProductList[index]);



                });
                cardbody2.appendChild(button2);



                fragment.appendChild(card);
            }
        }



    }
    product.appendChild(fragment);




}


function Eliminar(item) {


    var i = ProductList.indexOf(item);
    ProductList.splice(i, 1);
    localStorage.setItem("ListProducts", JSON.stringify(ProductList));
    const product = document.getElementById('Productos');
    product.innerHTML = '';
    productos();


}

function productselect(item) {
    if (item !== null) {
        localStorage.setItem("ProductSelect", JSON.stringify(item));
        let estado = true;
        localStorage.setItem("Editando", estado);
        location.href = 'http://sp.net/Crear_editar.html';
    }
    console.log(localStorage.getItem("ProductSelect"));
}




$(document).ready(function() {
    productos();
    name();
    cargarLista();



    // console.log(ProductList.splice(-2));


    $('#button-product').click(() => {
        let estado = false;
        localStorage.setItem("Editando", estado);

        location.href = 'http://sp.net/Crear_editar.html';

    });

    $('#button-nav').click(function() {

        $('#line1').hide();
        $('#line2').hide();


    })

    if (screen.width <= 375) {
        $('li').css({ 'margin': '-4px' });

    }


    $(window).resize(function() {


        if (screen.width <= 375) {
            $('li').css({ 'margin': '-4px' });

        }
        $('#line1').show();
        $('#line2').show();

    })






});